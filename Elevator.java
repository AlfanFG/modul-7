public class Elevator {
	private boolean doorOpen = false;
	private int currentFloor = 1;
	private final int TOP_FLOOR = 5;
	private final int BOTTOM_FLOOR = 1;
	public void openDoor(){
		//System.out.println("Opening door");
		doorOpen = true;
		//System.out.println("Door is open");
		
	}

	public void closeDoor(){
		System.out.println("Closing door.");
		this.doorOpen = false;
		System.out.println("Door is Closed");
	}

	public void goUp(){
		System.out.println("Going up one floor.");
		this.currentFloor++;
		System.out.println("Floor : " + currentFloor);
	}

	public void goDown(){
		System.out.println("Going down one floor.");
		this.currentFloor--;
		System.out.println("Floor : " + currentFloor);
	}

	public void setFloor(int desiredFloor){
		while(currentFloor != desiredFloor){
			if(currentFloor < desiredFloor){
				goUp();
			}else{
				goDown();
			}
		}
	}
	
	public int getFloor(){
		return currentFloor;
	}

	public boolean checkDoorStatus(){
		return doorOpen;
	}
	
}