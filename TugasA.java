import java.util.Scanner;
import java.awt.Color;
import binatang.*;
public class TugasA
{
	public static void main(String args[])
	{
		Lion singa = new Lion();
		Kangoroo kangguru = new Kangoroo();
		Horse kuda = new Horse();

		//SINGA
		singa.nama = "Leo";
		singa.warnaBulu =  new Color(0,1,1);
		singa.usia = 5;
		singa.bb = 5.5;
		singa.cetakInformasi();


		//Kangguru
		kangguru.nama = " boxi ";
		kangguru.warnaBulu = new Color(0,1,1);
		kangguru.usia = 4;
		kangguru.bb = 6.6;
		kangguru.jumlahAnak = 2;
		kangguru.cetakInformasi();


		//HORSE
		kuda.nama = "Alex";
		kuda.warnaBulu = new Color(0,0,0);
		kuda.usia = 3;
		kuda.bb = 5;
		kuda.statusJinak = true;
		kuda.kecepatanLari = 90;
		kuda.cetakInformasi();	

		//Anjing
		Scanner scan = new Scanner(System.in);
		Elevator myElevator = new Elevator();
		Dog buddy = new Dog();
		System.out.println("\n\n\nBerat Anjing Adalah : "+ buddy.getWeight());
		
	}
}