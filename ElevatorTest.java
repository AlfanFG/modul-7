public class ElevatorTest {
	public static void main(String[] args){
		boolean open = true;
		Elevator myElevator = new Elevator();
		myElevator.openDoor();
		myElevator.closeDoor();
		myElevator.goUp();
		myElevator.goUp();
		myElevator.goUp();
		myElevator.openDoor();
		myElevator.closeDoor();
		myElevator.goDown();
		myElevator.openDoor();
		myElevator.closeDoor();
		myElevator.goDown();
		myElevator.setFloor(5);
		myElevator.openDoor();
	}
}